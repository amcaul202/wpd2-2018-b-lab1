package uk.ac.gcu.katrinhartmann.messageboard;

import java.util.Scanner;

class Keyboard {
    Scanner keyboard;

    Keyboard() {
        keyboard = new Scanner(System.in);
    }

    int nextInt() {
        String line = keyboard.nextLine();
        int n;
        try{
            n = Integer.parseInt(line.trim());
        } catch (NumberFormatException e) {
            n = Integer.MIN_VALUE;
        }
        return n;
    }

    String nextLine() {
        return keyboard.nextLine();
    }
}
