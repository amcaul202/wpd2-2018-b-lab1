
package uk.ac.gcu.katrinhartmann.messageboard;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.logging.Logger;

import static org.junit.Assert.*;

public class TestTopic {
    static final Logger LOG = Logger.getLogger(TestTopic.class.getName());


    public TestTopic() {
    }

    @Before
    public void setUp() {

    }

    @Test
    public void testCreate() {
        String title = "Topic Title";
        Topic topic = new Topic(title);
        assertEquals(title, topic.getTitle());
    }

    @Test
    public void testAddMessage() {
        Topic topic = new Topic("Topic");
        Message message = new Message("Title", "text", new Date());
        topic.addMessage(message);
        assertEquals(1, topic.getMessages().size());

    }
}
