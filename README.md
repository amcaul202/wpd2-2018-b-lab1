#Simple Command Line Message Board


## Summary ##

* Sample implementation of the message board program for lab 1. For lab demonstration purposes only.
* Version 1

### Requirements ###

To run it you need [Java] and (optionally) [IntelliJ].

### Installation ###

* Clone the repo. Rebuild and run. 
* This is an IntelliJ project.
* The application runs from command line.

### Contribution guidelines ###

* You can extend the example, e.g. by adding functionality or unit tests.

### Who do I talk to? ###

* Highlight any questions in the lab(s) or in My Progress Blog.